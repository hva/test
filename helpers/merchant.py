# -*- coding: utf-8 -*-
"""
   Description:
        -
        -
"""

MERCHANT_ID_LENGTH = 8


class MerchantHelper:

    @staticmethod
    def merchant_id_formatted(merchant_id):
        if merchant_id is None:
            return None
        _merchant_id = str(merchant_id)

        if len(_merchant_id) >= MERCHANT_ID_LENGTH:
            return _merchant_id

        _delta = MERCHANT_ID_LENGTH - len(_merchant_id)
        return f'{"0" * _delta}{_merchant_id}'
