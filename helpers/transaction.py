import requests
from config import Config
from lib.exception import BadRequest
import csv
from datetime import datetime
from uuid import uuid4
import os
from .merchant import MerchantHelper
from utils import generate_checksum
from flask import request, Response
import json 
class TransactionHelper:
    def __init__(self) -> None:
        pass

    @staticmethod
    def dashboard_merchant(merchant_id, time, demo):
        # odoo.res_partner_pos_tidmid:merchant_id:1
        # data = redis.get(f'odoo.res_partner_pos_tidmid:merchant_id:{merchant_id}')
        # data = json.loads(data)
        data = {
            'customer_id': MerchantHelper.merchant_id_formatted(merchant_id)
        }
        data['time'] = int(time)
        if demo:
            data['demo'] = demo

        headers = {
            "Authorization": request.headers['Authorization'],
            'checksum': generate_checksum(data, request.headers['Authorization'].split()[1])
        }
        data = requests.get(url=Config.TRANSACTION_SERVICE_BASE_URL+'/dashboard-merchant', params=data, headers=headers)        
        if data.status_code == 400:
            raise BadRequest(errors = data.json()['errors'])
        return data.json()['data']
    
    @staticmethod
    def export_transaction(params):
        params['customer_id'] = MerchantHelper.merchant_id_formatted(params['customer_id'])
        headers = {
            "Authorization": request.headers['Authorization'],
            'checksum': generate_checksum(params, request.headers['Authorization'].split()[1])
        }
        response = requests.get(Config.TRANSACTION_SERVICE_BASE_URL+'/transactions', params=params, headers=headers)
        if response.status_code != 200:
            return Response(json.dumps(response.json()), status=400)
        file_name = str(uuid4()) +'.csv'
        f = open(file_name, 'w')
        csv_writer = csv.writer(f)
        csv_writer.writerow(['STT', 'Thời gian giao dịch', 'Mã đơn hàng', 'Số thẻ', 'Số tiền',
                            'Số tiền tip', 'Loại giao dịch', 'Trạng thái', 'Serial Number',])
        
        if 'items' in response.json()['data'].keys():
            rows = response.json()['data']['items']
            stt = 1
            _total = 0
            for row in rows:
                _total+=row['request_amount']
                csv_writer.writerow([
                    stt, 
                    datetime.fromtimestamp(row['original_transaction_date']),
                    row['invoice_no'],
                    row['card_no'],
                    row['request_amount'],
                    row['tip'],
                    row['transaction_type'],
                    'Thành công' if row['response_code'] == '00'  else 'Hủy' ,
                    row['serial_no'],
                ])
                stt+=1
            f.write("\n")
            f.write('Tổng cộng: ,'+str(_total))
        f.close()
        f = open(file_name, 'r')
        data = f.read()
        f.close()
        os.remove(file_name)
        return data
