# -*- coding: utf-8 -*-
"""
   Description:
        -
        -
"""
import os
from dotenv import load_dotenv

load_dotenv()


class Config:
    TMS_BASE_URL = os.getenv('TMS_BASE_URL')
    TRANSACTION_SERVICE_BASE_URL = os.getenv('TRANSACTION_SERVICE_BASE_URL')
    JWT_PUBLIC_KEY = open('public', 'r').read()

    REDIS_HOST = os.getenv('REDIS_HOST')
    REDIS_PORT = os.getenv('REDIS_PORT')
    # REDLOCK_REDIS = json.loads(os.getenv('REDLOCK_REDIS', '[]'))

    AUTH_ADDRESS = os.getenv('AUTH_ADDRESS', '')
    TRANSACTION_HOST = os.getenv('TRANSACTION_HOST')
