from flask import Flask
from flask_restful import Api
# from connect import app
from resources import api_resources, app_rerources
import connect

app = Flask(__name__)

api = Api(app)


@app.errorhandler(404)
def page_not_found(error):
    return {
               "msg": "The requested URL was not found on the server.",
               "data": {},
               "errors": [],
               "error_code": "E_NOT_FOUND"
           }, 404


@app.errorhandler(500)
def server_error_page(error):
    return {
               "msg": "Internal Server Error",
               "data": {},
               "errors": [],
               "error_code": "E_SERVER"
           }, 500

for prefix, resource in app_rerources.items():
    app.add_url_rule(prefix, view_func=resource)

# Add resource
for prefix, _resource in api_resources.items():
    api.add_resource(_resource, prefix)

if __name__ == '__main__':
    app.run(debug=True, port=5005, host='0.0.0.0')
