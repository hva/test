# -*- coding: utf-8 -*-
"""
   Description:
        -
        -
"""


class TMSService:
    def __init__(self, client):
        self.client = client

    def login(self, body):
        return self.client.post('/merchant-portal/login', json=body)

    def refresh_token(self, headers, body):
        return self.client.post('/merchant-portal/refresh', headers=headers, json=body)

    def validate_token(self, body):
        return self.client.post('/merchant-portal/check-token', json=body)
    
    def reset_password(self, body):
        return self.client.post('/merchant-portal/reset-pwd', json = body)
