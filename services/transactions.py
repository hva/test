# -*- coding: utf-8 -*-
"""
   Description:
        -
        -
"""

from utils import generate_checksum
from flask import request


class TransactionsService:
    def __init__(self, client):
        self.client = client

    def transactions_histories(self, params):
        headers = {
            "Authorization": request.headers['Authorization'],
            'checksum': generate_checksum(params, request.headers['Authorization'].split()[1])
        }
        return self.client.get('/transactions', params=params, headers = headers)
