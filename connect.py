# -*- coding: utf-8 -*-
"""
   Description:
        -
        -
"""
from redis import Redis

from config import Config
# from flask import Flask
# from flask_sqlalchemy import SQLAlchemy

from lib import HTTPSecurity
from lib.logger import debug

# db = SQLAlchemy()
#
# app = Flask(__name__)
#
# app.config['SQLALCHEMY_DATABASE_URI'] = Config.POSTGRES_URI
#
# db.init_app(app)


redis = Redis(
    host=Config.REDIS_HOST,
    port=Config.REDIS_PORT,
    decode_responses=True
)
# dlm = Redlock(Config.REDLOCK_REDIS, retry_count=2)

security = HTTPSecurity(redis=redis, auth_address=Config.AUTH_ADDRESS, jwt_public_key=Config.JWT_PUBLIC_KEY)
