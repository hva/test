import hashlib
from config import Config

def generate_checksum(request_data: dict, salt=''):
    _values = request_data.values()
    _values = [str(i) for i in _values]
    _values.sort()
    _values = salt+''.join(_values)
    return hashlib.md5(_values.encode()).hexdigest()

def generate_checksum_tms(data:str):
    _token = hashlib.sha256(data.encode('utf-8')).hexdigest()
    return _token