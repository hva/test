from marshmallow import Schema, fields, RAISE, validate
from lib import NotBlank

class ExportTransactionSchema(Schema):
    class Meta:
        unknown = RAISE
    serial_number = fields.String(required=False, default=None, allow_none=True, validate=NotBlank())
    status = fields.String(
        required=False,
        default="all",
        allow_none=True,
        validate=validate.OneOf(["success", "void", "error", "all"])
    )
    type = fields.String(
        required=False,
        default="all",
        allow_none=True,
        validate=validate.OneOf(["sale", "void", "pre_auth", "complete_pre_auth", "all"])
    )
    from_date = fields.String(required=False, default=None, allow_none=True, validate=NotBlank())
    to_date = fields.String(required=False, default=None, allow_none=True, validate=NotBlank())
