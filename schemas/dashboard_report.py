from marshmallow import Schema, fields

class DashboardReportSchema(Schema):
    time = fields.Integer(required=True)
    demo = fields.String(required=False, default='')