from marshmallow import Schema, EXCLUDE, fields, RAISE, validate

from lib import NotBlank


class TransactionHistoriesRequestSchema(Schema):
    class Meta:
        unknown = RAISE

    page = fields.Integer(required=False, default=1, validate=validate.Range(min=1))
    page_size = fields.Integer(required=False, default=10, validate=validate.Range(min=1))
    tid = fields.String(required=False, validate=NotBlank())
    mid = fields.String(required=False, validate=NotBlank())
    serial_number = fields.String(required=False, default=None, allow_none=True, validate=NotBlank())
    status = fields.String(
        required=False,
        default="all",
        allow_none=True,
        validate=validate.OneOf(["success", "void", "error", "all"])
    )
    type = fields.String(
        required=False,
        default="all",
        allow_none=True,
        validate=validate.OneOf(["sale", "void", "pre_auth", "complete_pre_auth", "all"])
    )
    query_key = fields.String(
        required=False,
        default="all",
        allow_none=True,
        validate=validate.OneOf(["transaction_id", "customer_id", "card_number", "all"])
    )
    query_value = fields.String(required=False, default=None, allow_none=True, validate=NotBlank())
    from_date = fields.String(required=False, default=None, allow_none=True, validate=NotBlank())
    to_date = fields.String(required=False, default=None, allow_none=True, validate=NotBlank())


class TransactionResponseSchema(Schema):
    class Meta:
        unknown = EXCLUDE

    id = fields.Integer(required=True)
    tid = fields.String(required=False, missing=None, allow_none=True)
    mid = fields.String(required=False, missing=None, allow_none=True)
    customer_id = fields.String(required=False, missing=None, allow_none=True)
    signature_data = fields.String(required=False, missing=None, allow_none=True)
    serial_no = fields.String(required=False, missing=None, allow_none=True)
    response_code = fields.String(required=False, missing=None, allow_none=True)
    transaction_type = fields.String(required=False, missing=None, allow_none=True)
    tip = fields.Integer(required=False, missing=0, allow_none=True)
    request_amount = fields.Float(required=False, missing=0.0, allow_none=True)
    card_no = fields.String(required=False, missing=None, allow_none=True)
    invoice_no = fields.String(required=False,  missing=None, allow_none=True)
    original_transaction_date = fields.Integer(required=False, missing=None, allow_none=True)


class TransactionHistoriesResponseSchema(Schema):
    class Meta:
        unknown = EXCLUDE
        ordered = True

    # {
    #     "items": result,
    #     'num_of_page': num_of_page,
    #     'page_size': page_size,
    #     'page': page
    # }
    items = fields.List(fields.Nested(TransactionResponseSchema), data_key='items', missing=[])
    num_of_page = fields.Integer(data_key='num_of_page', missing=0)
    page_size = fields.Integer(data_key='page_size', missing=10)
    page = fields.Integer(data_key='page', missing=1)
    total_request_amount = fields.Float(data_key='total_request_amount', missing=0)
    total_record = fields.Integer(data_key='total_record', missing=0)
