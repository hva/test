from marshmallow import Schema, EXCLUDE, RAISE, fields


class HelloSchema(Schema):
    field1 = fields.Str(required=True)
    field2 = fields.Email(required=False)
