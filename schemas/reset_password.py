from marshmallow import Schema, fields


class ResetPasswordSchema(Schema):
    email = fields.String(required=True)