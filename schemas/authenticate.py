from marshmallow import Schema, fields


class LoginRequestSchema(Schema):
    email = fields.Email(required=True)
    password = fields.Str(required=True)


class RefreshTokenRequestSchema(Schema):
    refresh_token = fields.Str(required=True)
