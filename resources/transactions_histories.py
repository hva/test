# -*- coding: utf-8 -*-
"""
   Description:
        -
        -
"""
import json

from flask import request
from flask_restful import Resource
from pydash import get

from config import Config
from connect import security
from helpers.merchant import MerchantHelper
from lib.logger import debug
from services.transactions import TransactionsService
from lib import ClientAPI, BadRequest
from schemas.transactions_histories import TransactionHistoriesRequestSchema, TransactionHistoriesResponseSchema


transaction_service_client = ClientAPI(host=Config.TRANSACTION_SERVICE_BASE_URL)
transaction_service_api = TransactionsService(client=transaction_service_client)


class TransactionHistoriesResource(Resource):

    @security.http(
        login_required=True,
        params=TransactionHistoriesRequestSchema(),
        # response=TransactionHistoriesResponseSchema()
    )
    def get(self, params, user):
        # _query = request.args.to_dict()
        # if 'checksum' not in request.headers:
        #     raise BadRequest(errors = ['checksum required'])
        #
        # if not security.checksum(security.get_token(), _query, request.headers['checksum']):
        #     raise BadRequest(errors = ['checksum invalid data'])
        
        print("query:", params)
        _merchant_id = get(user, 'merchant_id')
        _merchant_id_formatted = MerchantHelper.merchant_id_formatted(_merchant_id)

        print("merchant id:", _merchant_id)
        print("merchant id formatted:", _merchant_id_formatted)
        params['customer_id'] = _merchant_id_formatted
        params = {
            key: value for key, value in params.items() if value is not None
        }
        print("params formatted", params)
        _res = transaction_service_api.transactions_histories(params=params)

        if _res.status_code != 200:
            debug(f'Bad request: {_res.text}')
            _response_data = json.loads(_res.text)
            _msg = get(_response_data, 'msg')
            _errors = get(_response_data, 'errors')
            raise BadRequest(_msg, errors=_errors)

        # _data_mockup = {
        #     "items": [
        #         {
        #             "id": 28029,
        #             "tid": "00000136",
        #             "customer_id": "00000010",
        #             "serial_no": "000023742633",
        #             "invoice_no": "001265",
        #             "tip": 0,
        #             "original_transaction_date": 1668251643,
        #             "transaction_type": "sale",
        #             "request_amount": 999999999.0,
        #             "response_code": "00",
        #             "mid": "000000040100034",
        #             "signature_data": None,
        #             "card_no": "47617390*0119"
        #         },
        #         {
        #             "id": 28164,
        #             "tid": "00000136",
        #             "customer_id": "00000010",
        #             "serial_no": "000023742633",
        #             "invoice_no": "001265",
        #             "tip": 0,
        #             "original_transaction_date": 1668251961,
        #             "transaction_type": "sale",
        #             "request_amount": 999999999.0,
        #             "response_code": "00",
        #             "mid": "000000040100034",
        #             "signature_data": None,
        #             "card_no": "47617390*0119"
        #         }
        #     ],
        #     "num_of_page": 18,
        #     "page_size": 2,
        #     "page": 1,
        #     "total_request_amount": 8001039998.0,
        #     "total_record": 35
        # }

        _data = _res.json()['data']
        # if not get(_data, 'items', []):
        #     _data = _data_mockup

        return {
            **_data
        }
