# -*- coding: utf-8 -*-
"""
   Description:
        - 
        -
"""
from resources.hello import HelloWorldResource
from resources.health_check import HealthCheckResource
from resources.dashboard_merchant import DashboardMerchant
from resources.export_transaction import export_transaction
from resources.login import LoginResource
from resources.merchant_info import MerchantInfoResource
from resources.transactions_histories import TransactionHistoriesResource
from resources.refresh_token import RefreshTokenResource
from resources.reset_password import ResetPassword

api_resources = {
    '/hello': HelloWorldResource,
    '/common/health_check': HealthCheckResource,
    '/dashboard_report': DashboardMerchant,
    '/authenticate/login': LoginResource,
    '/authenticate/refresh_token': RefreshTokenResource,
    '/transactions': TransactionHistoriesResource,
    '/merchant_info': MerchantInfoResource,
    '/reset-password': ResetPassword
    # **{f'/iapi{k}': val for k, val in iapi_resources.items()},
}

app_rerources = {
    '/export_transaction': export_transaction
}