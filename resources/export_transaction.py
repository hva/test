from helpers.transaction import TransactionHelper
from flask import Response
from datetime import datetime
from flask import request
import jwt
import json
from connect import redis
from schemas.export_transaction import ExportTransactionSchema


def export_transaction():
    try:
        _token = request.headers['Authorization'].split()[1]
        payload = jwt.decode(_token, algorithm="RS256", options={
                             "verify_signature": False})['payload']
    except:
        return Response(
            json.dumps({
                "data": "",
                "errors": ["user invalid"],
                "msg": "",
                "error_code": "Bad Request"
            }), status=400)

    schema = ExportTransactionSchema()
    _response_validate = schema.validate(request.args)
    if _response_validate:
        return Response(json.dumps({
            "data": "",
            "errors": _response_validate,
            "msg": "",
            "error_code": "Bad Request"
        }), status=400)

    params = {'page_size': 100000, **request.args}
    params['customer_id'] = payload['merchant_id']

    data = TransactionHelper.export_transaction(params)
    if isinstance(data, Response):
        return data

    _redis_key = f"odoo:res_partner_pos_tidmid:merchant_id:{payload['merchant_id']}"
    _redis_value = redis.get(_redis_key)
    _merchant_info = json.loads(_redis_value)

    file_name = datetime.now().strftime('%H:%M:%S %d-%m-%Y') + \
        f"_{_merchant_info['merchant_name']}.csv"
    return Response(
        data,
        mimetype="text/csv",
        headers={
            "Content-disposition": f"attachment; filename={file_name}",
            "Content-Type": "application/octet-stream"
        })
