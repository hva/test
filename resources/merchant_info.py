# -*- coding: utf-8 -*-
"""
   Description:
        -
        -
"""
import json

from flask_restful import Resource
from pydash import get

from connect import security, redis
from lib import BadRequest


class MerchantInfoResource(Resource):

    @security.http(
        login_required=True
    )
    def get(self, user):
        _merchant_id = get(user, 'merchant_id')
        _merchant_info_key = f'odoo:res_partner_pos_tidmid:merchant_id:{_merchant_id}:hset'
        print("tid_mid_key:", _merchant_info_key)
        _merchant_info = redis.hgetall(_merchant_info_key)

        print("merchant_info before format:", _merchant_info)

        if _merchant_info is None:
            raise BadRequest("Invalid data.", errors=["Merchant id not found."])

        _merchant_info = [json.loads(item) for item in _merchant_info.values()]

        print("merchant_info after format:", _merchant_info)
        _serials = []
        _tids = []
        _mids = []

        for item in _merchant_info:
            _serial = get(item, 'serial_number', '')
            _tid = get(item, 'tid', '')
            _mid = get(item, 'mid', '')

            if _serial not in _serials:
                _serials.append(_serial)
            if _tid not in _tids:
                _tids.append(_tid)
            if _mid not in _mids:
                _mids.append(_mid)

        return {
            "merchant_id": _merchant_id,
            "merchant_name": get(_merchant_info, '0.merchant_name', ''),
            "serial_number": _serials,
            "tid": _tids,
            "mid": _mids,
        }
