# -*- coding: utf-8 -*-
"""
   Description:
        -
        -
"""
import json

from flask_restful import Resource
from pydash import get

from config import Config
from connect import security
from services.tms import TMSService
from lib import ClientAPI, BadRequest
from lib.logger import debug
from schemas.authenticate import LoginRequestSchema
from utils import generate_checksum_tms

tms_client = ClientAPI(host=Config.TMS_BASE_URL)
tms_api = TMSService(client=tms_client)


class LoginResource(Resource):

    @security.http(
        form_data=LoginRequestSchema()
    )
    def post(self, form_data):
        _email = get(form_data, 'email')
        _password = get(form_data, 'password')

        _res = tms_api.login(body={
            "params": {
                "email": _email,
                "password": _password,
                "checksum": generate_checksum_tms(f'{_email}-{_password}')
            }
        })

        if _res.status_code != 200:
            debug(f'Status {_res.status_code}')
            debug(f'Error {_res.text}')
            raise BadRequest("An occur error.", errors=["Service error"])

        _result = json.loads(_res.text)['result']
        print("Call TMS login:", _result)
        _msg = get(_result, 'msg', '')
        _access_token = get(_result, 'data.token', '')
        _refresh_token = get(_result, 'data.refresh_token', '')
        if _msg:
            raise BadRequest("Invalid data.", errors=[_msg])
        if not _access_token:
            raise BadRequest("An occur error.", errors=["Service error"])
        if not _refresh_token:
            raise BadRequest("An occur error.", errors=["Service error"])

        return {
            "access_token": _access_token,
            "refresh_token": _refresh_token
        }
