# -*- coding: utf-8 -*-
"""
   Description:
        -
        -
"""
import json

from flask import request
from flask_restful import Resource
from pydash import get

from config import Config
from connect import security
from services.tms import TMSService
from lib import ClientAPI, BadRequest, Forbidden
from lib.logger import debug
from schemas.authenticate import RefreshTokenRequestSchema
from utils import generate_checksum_tms

tms_client = ClientAPI(host=Config.TMS_BASE_URL)
tms_api = TMSService(client=tms_client)


class RefreshTokenResource(Resource):
    @security.http(
        form_data=RefreshTokenRequestSchema()
    )
    def post(self, form_data):

        _refresh_token = get(form_data, 'refresh_token')
        _authorization = get(request.headers, 'Authorization', None)

        print("auth", _authorization)

        if _authorization is None:
            raise Forbidden(
                errors=[{
                    "access_token": "Invalid"
                }],
                error_code='REFRESH_TOKEN_INVALID'
            )

        _res = tms_api.refresh_token(
            headers={
                "Authorization": _authorization
            },
            body={
                "params": {
                    "refresh_token": _refresh_token,
                    "checksum": generate_checksum_tms(f'{_authorization.split()[1]}-{_refresh_token}')
                }
            }
        )

        if _res.status_code != 200:
            debug(f'Status {_res.status_code}')
            debug(f'Error {_res.text}')
            raise BadRequest("An occur error.", errors=["Service error"])

        print(_res.text)

        _result = json.loads(_res.text)['result']
        if _result is None:
            raise BadRequest("An occur error.", errors=["Service error"])

        print("Call TMS refresh_token:", _result)
        _msg = get(_result, 'msg', '')
        _access_token = get(_result, 'data.token', '')
        _refresh_token = get(_result, 'data.refresh_token', '')
        if _msg:
            raise Forbidden(errors=[_msg], error_code='REFRESH_TOKEN_INVALID')
        if not _access_token:
            raise BadRequest("An occur error.", errors=["Service error"])
        if not _refresh_token:
            raise BadRequest("An occur error.", errors=["Service error"])

        return {
            "access_token": _access_token,
            "refresh_token": _refresh_token
        }
