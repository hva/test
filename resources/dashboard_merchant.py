from flask_restful import Resource
from connect import security
from helpers.transaction import TransactionHelper
from schemas.dashboard_report import DashboardReportSchema

class DashboardMerchant(Resource):

    @security.http(
        login_required=True,
        params=DashboardReportSchema(),
        # is_checksum=True
    )
    def get(self, params, user):
        merchant_id = user['merchant_id']
        data = TransactionHelper.dashboard_merchant(merchant_id, params['time'], params['demo'])      
        return data
    