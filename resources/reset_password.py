from flask_restful import Resource
from connect import security
from schemas.reset_password import ResetPasswordSchema
from services.tms import TMSService
from requests import Response
from utils import generate_checksum_tms
from lib import ClientAPI, BadRequest
from config import Config


tms_client = ClientAPI(host=Config.TMS_BASE_URL)
tms_api = TMSService(client=tms_client)


class ResetPassword(Resource):

    @security.http(
        form_data=ResetPasswordSchema()
    )
    def post(self, form_data):
        _request_data = {
            "params":{
                **form_data,
                'checksum': generate_checksum_tms(form_data['email'])
            }
        }
        _response:Response = tms_api.reset_password(_request_data)
        _data = _response.json()

        if _data['result']['data']['reset_pwd']:
            return {"result": True}
        
        raise BadRequest(errors = [_data['result']['msg']])


