from connect import db


class HelloModel(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.String)

    @property
    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
